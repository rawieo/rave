use std::rc::Rc;
use std::cell::Cell;
use libc;

pub struct StackFrame<T> {
  data: T,
  prev: Box<StackFrame<T>>
}

