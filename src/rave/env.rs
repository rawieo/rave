use std::collections::HashMap;
use rave::instruction::Instruction;

pub struct DefinitionSource {

}

pub struct RaveEnv {
  type_names: HashMap<u64, String>,
  methods: HashMap<u64, Vec<Instruction>>,
  source_map: HashMap<(u64, u64), DefinitionSource>
}
